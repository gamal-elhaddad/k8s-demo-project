###### 

# k8s-Demo-Project, Deploying Mongo-Express application and connect it with the MongoDB database in kubernetes cluster.

🔍Step1:Create Secrets (username and password) to connect to the database by configure mongo-secret.yaml.
   * kubectl apply -f mongo-secret.yaml

🔍Step2:Create a MongoDB Deployment and create an internal service for MongoDB in one step by configure mongo.yaml file.
   * kubectl apply -f mongo.yaml

🔍Step3:Create ConfigMap for Mongo-Express App by configure mongo-configmap.yaml file.
   * kubectl apply -f mongo-configmap.yaml 
    
🔍Step4:Create a Mongo-Express Deployment and create an external service for Mongo-Express App in one step by configure mongo-express.yaml file.
   * kubectl apply -f  mongo-express.yaml

🔍Step5:Give a URL to external service in minikube.
   * minikube service mongo-express-service

######   





   




